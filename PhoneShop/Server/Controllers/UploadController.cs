﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace PhoneShop.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UploadController : ControllerBase
    {
        private readonly IWebHostEnvironment _environment;
        
        public UploadController(IWebHostEnvironment environment)
        {
            _environment = environment;
        }


        [HttpPost]
        public async Task Post()
        {
            try
            {
                if (HttpContext.Request.Form.Files.Any())
                {
                    foreach (var file in HttpContext.Request.Form.Files)
                    {
                        string tmp = getFileLocation(_environment.ContentRootPath);
                        var path = Path.Combine(tmp, "images", file.FileName);
                        using (var stream = new FileStream(path, FileMode.Create))
                        {
                            await file.CopyToAsync(stream);
                        }
                    }
                }
            } catch(Exception e)
            {
                Console.WriteLine("error" + e);
            }
            
        }

        //"D:\\CN5\\PRN292\\webdotnet\\PhoneShop\\Server"
        //D:\CN5\PRN292\webdotnet\Crawler\build
         private String getFileLocation(String contentRootPath)
        {
            try
            {
                contentRootPath = contentRootPath.Substring(0, 
                    contentRootPath.IndexOf("\\PhoneShop")) + "\\Crawler\\build";
                return contentRootPath;
            }
            catch (Exception e)
            {
                return "";
            }
        }
    }
}
