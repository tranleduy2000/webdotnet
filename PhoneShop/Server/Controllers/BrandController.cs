﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PhoneShop.Server.Repository;
using PhoneShop.Shared.Model;

namespace PhoneShop.Server.Controllers
{
    [Route("api/brand")]
    [ApiController]
    public class BrandController : ControllerBase
    {
        private readonly IBrandRepository _brandRepository;

        public BrandController(IBrandRepository brandRepository)
        {
            _brandRepository = brandRepository;
        }

        /// <summary>
        /// /api/brand
        /// </summary>
        /// <returns></returns>
        [HttpGet("")]
        [AllowAnonymous]
        public IEnumerable<Brand> GetAllBrand()
        {
            return _brandRepository.GetAllBrand();
        }
        
        /// <summary>
        /// /api/brand/id?brandId=id
        /// </summary>
        /// <param name="brandId"></param>
        /// <returns></returns>
        [HttpGet("id")]
        [AllowAnonymous]
        public Brand GetBrand(int brandId)
        {
            return _brandRepository.GetBrand(brandId);
        }

        [HttpPost]
        [Jwt.Authorize(Role.Admin)]
        public Brand AddBrand(Brand brand)
        {
            return _brandRepository.AddBrand(brand);
        }

        [Jwt.Authorize(Role.Admin)]
        [HttpPut]
        public Brand UpdateBrand(Brand brand)
        {
            return _brandRepository.UpdateBrand(brand);
        }

        [Jwt.Authorize(Role.Admin)]
        [HttpDelete]
        public IActionResult DeleteBrand(int brandId)
        {
            var success = _brandRepository.DeleteBrand(brandId);
            if (success)
            {
                return new OkResult();
            }
            return new BadRequestResult();
        }
        
    }
}
