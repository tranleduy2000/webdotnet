﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using PhoneShop.Server.Data;
using PhoneShop.Server.Extensions;
using PhoneShop.Server.Repository;
using PhoneShop.Shared.Model;

namespace PhoneShop.Server.Controllers
{
    [Route("api/cart")]
    [ApiController]
    public class CartController : ControllerBase
    {
        private readonly IProductRepository _productRepository;
        private readonly IOrderRepository _orderRepository;

        public CartController(IProductRepository productRepository, IOrderRepository orderRepository)
        {
            _productRepository = productRepository;
            _orderRepository = orderRepository;
        }
        
        [HttpGet("")]
        public Cart Get()
        {
            var cart = HttpContext.Session.GetObject<Cart>("cart");
            if (cart == null)
            {
                cart = new Cart();
                Set(cart);
            }
            return cart;
        }
        
        [HttpPut("update")]
        private void Set(Cart cart)
        {
            HttpContext.Session.SetObject("cart", cart);
        }

        [HttpPut("")]
        public void Add(int productId)
        {
            var cart = Get();
            var cartItem = cart.Get(productId);
            if (cartItem == null)
            {
                cartItem = new Cart.CartItem(_productRepository.GetProduct(productId), 0);
                cart.Add(cartItem);
            }

            cartItem.Quantity += 1;
            Set(cart);
        }

        [HttpPut("updateOneItem")]
        public void Update(int productId, int quantity)
        {
            Cart cart = Get();
            var cartItem = cart.Get(productId);
            if (cartItem != null)
            {
                if (quantity == 0)
                {
                    cart.Remove(productId);
                }
                else
                {
                    cartItem.Quantity = quantity;
                }
            }
            else
            {
                Add(productId);
                Update(productId, quantity);
            }
            Set(cart);
        }

        [HttpDelete("")]
        public void Delete(int productId)
        {
            Cart cart = Get();
            cart.Remove(productId);
            Set(cart);
        }
        
        [HttpGet("checkstock")]
        public bool CheckStock()
        {
            Cart cart = HttpContext.Session.GetObject<Cart>("cart") ?? new Cart();
            if (cart.CartItems.Count == 0)
            {
                return false;
            }
            return _orderRepository.CheckStock(cart);
        }

    }
}