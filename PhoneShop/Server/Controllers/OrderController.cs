﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using PhoneShop.Server.Extensions;
using PhoneShop.Server.Repository;
using PhoneShop.Shared.Model;

namespace PhoneShop.Server.Controllers
{
    [Route("api/order")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        private readonly IOrderRepository _orderRepository;
        private readonly IUsersRepository _usersRepository;

        public OrderController(IOrderRepository orderRepository, IUsersRepository usersRepository)
        {
            _orderRepository = orderRepository;
            _usersRepository = usersRepository;
        }

        /// <summary>
        /// Implement endpoint */api/order
        /// Example: https://localhost:5001/api/order
        /// </summary>
        /// <returns>Return all orders</returns>
        [Jwt.Authorize]
        [HttpGet("")]
        public IActionResult GetAllOrders()
        {
            var currentUser = GetCurrentUser();
            if (currentUser == null)
            {
                return new UnauthorizedResult();
            }

            if (currentUser.RoleId == Role.AdminId)
            {
                return Ok(_orderRepository.GetAllOrders());
            }

            return Ok(_orderRepository.GetOrdersByUserId(currentUser.Id));
        }

        /// <summary>
        /// Implement endpoint */api/order/byId?orderId=orderId
        /// </summary>
        /// <param name="orderId">Order id</param>
        /// <returns>Order json object</returns>
        [AllowAnonymous]
        [HttpGet("byId")]
        public IActionResult GetOrder(int orderId)
        {
            var order = _orderRepository.GetOrder(orderId);
            if (order.UserId != null)
            {
                Users user = GetCurrentUser();
                if (user == null || user.Id != order.UserId)
                {
                    return new UnauthorizedResult();
                }
            }
            if (order.User != null)
            {
                order.User = null; // remove ref
            }
            order.OrderDetail = null; // remove ref
            return Ok(order);
        }

        /// <summary>
        /// Implement endpoint */api/order/byUser?userId=userId
        /// Example: https://localhost:5001/api/order/byUser?userId=duy
        /// </summary>
        /// <param name="userId"></param>
        /// <returns>Return orders which belong to userId</returns>
        [Jwt.Authorize]
        [HttpGet("byUser")]
        public IActionResult GetOrders(string userId)
        {
            var currentUser = GetCurrentUser();
            if (currentUser != null)
            {
                if (currentUser.RoleId == Role.AdminId || currentUser.Id == userId)
                {
                    return Ok(_orderRepository.GetOrdersByUserId(userId));
                }
            }

            return new UnauthorizedResult();
        }

        /// <summary>
        /// Implement endpoint */api/order/details?orderId=orderId
        /// Example: https://localhost:5001/api/order/details?orderId=1
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns>List of order detail objects belong to orderId</returns>
        [AllowAnonymous]
        [HttpGet("details")]
        public IActionResult GetOrderDetails(int orderId)
        {
            var order = _orderRepository.GetOrder(orderId);
            if (order.UserId == null)
            {
                return Ok(_orderRepository.GetOrderDetails(orderId));
            }

            var currentUser = GetCurrentUser();
            if (currentUser != null)
            {
                if (currentUser.RoleId == Role.AdminId || order.UserId == currentUser.Id)
                {
                    return Ok(_orderRepository.GetOrderDetails(orderId));
                }
            }

            return new ForbidResult();
        }


        /// <summary>
        /// Perform checkout process, generate order and insert them into database.
        /// Example:
        /// request url: */api/order/checkout
        /// body:
        /// {
        ///    "FullName" : "Tran Le Duy",
        ///    "Address" : "HCMC, 9 District, 494 street",
        ///    "Phone": "0926128832",
        ///    "UserId": "duy",
        ///    "PaymentMethod": "COD"
        /// }
        /// </summary>
        /// <param name="orderInfo"></param>
        /// <returns>if action successes then return orderId, otherwise return null.</returns>
        [AllowAnonymous]
        [HttpPost("checkout")]
        public string CheckOutCart([FromBody] OrderInfo orderInfo)
        {
            Cart cart = HttpContext.Session.GetObject<Cart>("cart") ?? new Cart();
            if (cart.CartItems.Count == 0)
            {
                return null;
            }

            var currentUser = GetCurrentUser();
            if (currentUser != null)
            {
                orderInfo.UserId = currentUser.Id;
            }

            var orders = _orderRepository.InsertOrder(cart, orderInfo);
            if (orders != null)
            {
                HttpContext.Session.SetObject("cart", null);
                return orders.Id.ToString();
            }

            return null;
        }

        private Users GetCurrentUser()
        {
            string userid = HttpContext.User.FindFirstValue("userId");
            Users user = _usersRepository.GetUser(userid);
            return user;
        }
    }
}