﻿using System.Collections.Generic;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PhoneShop.Server.Jwt;
using PhoneShop.Server.Repository;
using PhoneShop.Shared.Model;

namespace PhoneShop.Server.Controllers
{
    [Route("api/product")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IProductRepository _productRepository;
        private readonly IUsersRepository _usersRepository;

        public ProductController(IProductRepository productRepository, IUsersRepository usersRepository)
        {
            _productRepository = productRepository;
            _usersRepository = usersRepository;
        }

        [AllowAnonymous]
        [HttpGet("")]
        public IEnumerable<Product> GetAllProducts()
        {
            IEnumerable<Product> products = _productRepository.GetAllProduct();
            return products;
        }

        /// <summary>
        /// Implement endpoint */api/product/byId?id=id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Product json object</returns>
        [AllowAnonymous]
        [HttpGet("byId")]
        public Product GetProduct(int id)
        {
            return _productRepository.GetProduct(id);
        }

        [AllowAnonymous]
        [HttpGet("keyword")]
        public IEnumerable<Product> GetAllProducts(string keyword, double minPrice,
            double maxPrice, int brandId, int page)
        {
            var currentUser = GetCurrentUser();
            if (currentUser != null && currentUser.RoleId == Role.AdminId)
            {
                return _productRepository.GetFilterProductAdmin(keyword, minPrice, maxPrice, brandId, page);
            }
            return _productRepository.GetFilterProduct(keyword, minPrice, maxPrice, brandId, page);
        }

        [Jwt.Authorize(Role.Admin)]
        [HttpPost("")]
        public Product AddProduct(Product product)
        {
            return _productRepository.AddProduct(product);
        }

        [Jwt.Authorize(Role.Admin)]
        [HttpPut]
        public Product UpdateProduct(Product product)
        {
            return _productRepository.UpdateProduct(product);
        }

        private Users GetCurrentUser()
        {
            string userid = HttpContext.User.FindFirstValue("userId");
            Users user = _usersRepository.GetUser(userid);
            return user;
        }
    }
}