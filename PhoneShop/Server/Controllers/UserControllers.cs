﻿// using Microsoft.AspNetCore.Authorization;

using System;
using Microsoft.AspNetCore.Mvc;
using PhoneShop.Server.Repository;
using PhoneShop.Shared.Model;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.IdentityModel.Tokens;
using PhoneShop.Server.Jwt;
using static PhoneShop.Shared.Model.Role;
using ControllerBase = Microsoft.AspNetCore.Mvc.ControllerBase;

namespace PhoneShop.Server.Controllers
{
    [Route("api/users")]
    [ApiController]
    public class UserControllers : ControllerBase
    {
        private readonly IUsersRepository _usersRepository;

        public UserControllers(IUsersRepository usersRepository)
        {
            _usersRepository = usersRepository;
        }

        [AllowAnonymous]
        [HttpPost("authenticate")]
        public IActionResult Login([FromBody] UserLoginModel userLoginModel)
        {
            // UserLoginModel userLoginModel = new UserLoginModel(userName, password);
            AuthenticateResponse authenticateResponse = CheckLogin(userLoginModel);

            if (authenticateResponse == null)
            {
                return BadRequest(new {message = "User name or password is incorrect"});
            }

            //Add token to 
            HttpContext.Session.SetString("JWToken", authenticateResponse.Token);

            //Add role to context for authenticate
            var userClaims = HttpContext.User.Claims.ToList();
            string userRole = GetRoleName(authenticateResponse.RoleId);
            userClaims.Add(new Claim("role", userRole));

            HttpContext.Items["User"] = _usersRepository.GetUser(authenticateResponse.UserId);
            return Ok(authenticateResponse);
        }

        //Get all users => list
        [Jwt.Authorize(Admin)]
        [HttpGet("")]
        public IEnumerable<Users> GetAllUsers()
        {
            return _usersRepository.GetAllUsers();
            ;
        }

        /// <summary>
        /// Implement endpoint: /api/users/logout
        /// </summary>
        /// <returns></returns>
        [Jwt.Authorize]
        [HttpGet("logout")]
        public IActionResult LogOut()
        {
            HttpContext.Session.Clear();
            return new OkResult();
        }


        /// <summary>
        /// Implement endpoint: /api/users/currentuser
        /// </summary>
        /// <returns>Return current login-ed user</returns>
        [HttpGet("currentuser")]
        public IActionResult GetCurrentUser()
        {
            string userid = HttpContext.User.FindFirstValue("userId");
            Users user = _usersRepository.GetUser(userid);
            if (user == null)
            {
                return new UnauthorizedResult();
            }

            user.Password = null;
            return Ok(user);
        }

        //Create account
        [AllowAnonymous]
        [HttpPost("register")]
        public IActionResult AddNewUser(NewAccount newAccount)
        {
            Users user = new Users();
            user.Address = newAccount.Address;
            user.Id = newAccount.Id;
            user.Password = newAccount.Password;
            user.RoleId = UserId;
            user.Phone = newAccount.Phone;
            user.FullName = newAccount.FullName;

            Users newUser = _usersRepository.AddUser(user);
            bool success = newUser != null;
            if (success)
            {
                return Ok();
            }

            return new BadRequestResult();
        }


        /// <summary>
        /// Implement endpoint: /api/users
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        //Update user api
        [Jwt.Authorize]
        [HttpPut]
        public IActionResult UpdateUser(Users user)
        {
            user = _usersRepository.UpdateUser(user);
            return Ok(user);
        }

        /// <summary>
        /// Implement endpoint: /api/users
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        //Delete user
        [Jwt.Authorize(Admin)]
        [HttpDelete]
        public IActionResult DeleteUser(string userName)
        {
            var result = _usersRepository.DeleteUser(userName);
            if (result)
            {
                return new OkResult();
            }
            return new BadRequestResult();
        }

        //Get user by id function 
        //EX: /api/users/getuserbyid?username=nam
        [Jwt.Authorize(Admin)]
        [HttpGet("getuserbyid")]
        public IActionResult GetUser(string username)
        {
            Users user = _usersRepository.GetUser(username);
            if (user == null)
            {
                return NotFound();
            }
            return Ok(user);
        }

        //Change password user's account
        [Jwt.Authorize(Admin, Role.User)]
        [HttpPost("changepassword")]
        public IActionResult ChangePassword(PasswordModel passwordModel)
        {
            string userid = HttpContext.User.FindFirstValue("userId");
            bool success = _usersRepository.UpdatePassword(userid, 
                passwordModel.OldPassword,
                passwordModel.NewPassword);
            if (success)
            {
                LogOut();
                return new OkResult();
            }
            return new BadRequestResult();
        }


        //Change info user's account
        [Jwt.Authorize(Admin, Role.User)]
        [HttpPost("changeInfo")]
        public IActionResult ChangeInfo(AccountChangingInfoModel accountChangingInfoModel)
        {
            string userid = HttpContext.User.FindFirstValue("userId");
            Users user = _usersRepository.GetUser(userid);
            user.FullName = accountChangingInfoModel.FullName;
            user.Address = accountChangingInfoModel.Address;
            user.Phone = accountChangingInfoModel.Phone;
            _usersRepository.UpdateUser(user);
            return new OkResult();
        }
        
        public AuthenticateResponse CheckLogin(UserLoginModel userLoginModel)
        {
            Users user  = _usersRepository.CheckLogin(userLoginModel.UserName, userLoginModel.Password);
            if (user == null)
            {
                return null;
            }
            user.WithoutPassword();
            var token = GenerateJwtToken(user);
            return new AuthenticateResponse(user, token);
        }

        private string GenerateJwtToken(Users user)
        {
            // generate token that is valid for 7 days
            var tokenHandler = new JwtSecurityTokenHandler();

            var key = Encoding.ASCII.GetBytes(AppSettings.Secret);

            string role = GetRoleName(user.RoleId.Value);

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[]
                    {
                        new Claim("userId", user.Id),
                        new Claim("userRole", role)
                    }
                ),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key),
                    SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }
    }
}