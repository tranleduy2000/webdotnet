﻿using PhoneShop.Shared.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhoneShop.Server.Repository
{
    public interface IBrandRepository
    {
        public IEnumerable<Brand> GetAllBrand();
        public Brand AddBrand(Brand brand);
        public Brand UpdateBrand(Brand brand);
        public bool DeleteBrand(int brandId);
        public Brand GetBrand(int brandId);
    }
}