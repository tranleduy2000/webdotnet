﻿using PhoneShop.Shared.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhoneShop.Server.Repository
{
    interface ICartRepository
    {
        public Cart Get();
        public void  Add(int productId);
        public bool Update(int productId, int quantity);
        public bool Delete(int productId);
        public void Set();
    }
}
