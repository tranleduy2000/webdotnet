﻿using PhoneShop.Shared.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhoneShop.Server.Repository
{
    interface IOrderDetailRepository
    {
        public IEnumerable<OrderDetail> GetOrderDetailsByOrderId(int OrderId);
        public bool InsertOrderDetail(List<OrderDetail> orderDetails);
    }
}
