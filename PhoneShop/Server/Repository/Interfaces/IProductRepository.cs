﻿using PhoneShop.Shared.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhoneShop.Server.Repository
{
    public interface IProductRepository
    {
        public IEnumerable<Product> GetAllProduct();

        public IEnumerable<Product> GetFilterProduct(string keyword, double minPrice, double maxPrice, int brandId,
            int page);

        public IEnumerable<Product> GetFilterProductAdmin(string keyword, double minPrice, double maxPrice, int brandId,
            int page);

        public Product GetProduct(int productId);

        public Product AddProduct(Product product);
        public Product UpdateProduct(Product product);
    }
}