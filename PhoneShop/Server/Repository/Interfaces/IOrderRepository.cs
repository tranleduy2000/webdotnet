﻿using PhoneShop.Shared.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhoneShop.Server.Repository
{
    public interface IOrderRepository
    {
        public IEnumerable<Orders> GetAllOrders();

        public IEnumerable<Orders> GetOrdersByUserId(string userId);

        public Orders GetOrder(int orderId);

        public IEnumerable<OrderDetail> GetOrderDetails(int orderId);

        Orders InsertOrder(Cart cart, OrderInfo orderInfo);

        /// <summary>
        /// Check if the number of products in database is enough for create order
        /// </summary>
        bool CheckStock(Cart cart);
    }
}