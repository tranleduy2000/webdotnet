﻿using PhoneShop.Shared.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PhoneShop.Server.Jwt;

namespace PhoneShop.Server.Repository
{
    public interface IUsersRepository
    {
        public IEnumerable<Users> GetAllUsers();
        public Users AddUser(Users user);
        public Users UpdateUser(Users user);
        public bool DeleteUser(string userName);
        public Users GetUser(string userName);
        public Users CheckLogin(string userName, string password);
        public bool UpdatePassword(string userName, string oldPassword, string newPassword);
    }
}