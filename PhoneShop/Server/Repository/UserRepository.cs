﻿using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using PhoneShop.Server.Data;
using PhoneShop.Server.Jwt;
using PhoneShop.Shared.Model;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;

namespace PhoneShop.Server.Repository
{
    public class UserRepository : IUsersRepository
    {
        private readonly ApplicationDbContext _appDbContext;

        public UserRepository(ApplicationDbContext applicationDb)
        {
            _appDbContext = applicationDb;
        }

        public Users AddUser(Users user)
        {
            try
            {
                Users currentUser = GetUser(user.Id);
                if (currentUser == null)
                {
                    user.Status = true;
                    _appDbContext.Users.Add(user);
                    _appDbContext.SaveChanges();
                    return user;
                }
            }
            catch
            {
                // ignored
            }

            return null;
        }

        public bool DeleteUser(string userName)
        {
            try
            {
                Users user = _appDbContext.Users.Find(userName);
                user.Status = false;
                _appDbContext.Users.Update(user);
                _appDbContext.SaveChanges();

                _appDbContext.Users.Remove(_appDbContext.Users.Find(userName));
                _appDbContext.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
        }

        public Users CheckLogin(string userName, string password)
        {
            return _appDbContext.Users
                .FirstOrDefault(users => users.Id == userName && users.Password == password);
        }

        public bool UpdatePassword(string userName, string oldPassword, string newPassword)
        {
            var user = CheckLogin(userName, oldPassword);
            if (user == null)
            {
                return false;
            }
            user.Password = newPassword;
            _appDbContext.Users.Update(user);
            var success = _appDbContext.SaveChanges() > 0;
            return success;
        }

        public IEnumerable<Users> GetAllUsers()
        {
            return _appDbContext.Users.ToList();
        }

        public Users GetUser(string userName)
        {
            return _appDbContext.Users.Find(userName);
        }

        public Users UpdateUser(Users user)
        {
            _appDbContext.Users.Update(user);
            _appDbContext.SaveChanges();
            return user;
        }
    }
}