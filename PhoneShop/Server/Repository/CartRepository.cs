using PhoneShop.Shared.Model;

namespace PhoneShop.Server.Repository
{
    public class CartRepository : ICartRepository
    {
        public Cart Get()
        {
            throw new System.NotImplementedException();
        }

        public void Add(int productId)
        {
            throw new System.NotImplementedException();
        }

        public bool Update(int productId, int quantity)
        {
            throw new System.NotImplementedException();
        }

        public bool Delete(int productId)
        {
            throw new System.NotImplementedException();
        }

        public void Set()
        {
            throw new System.NotImplementedException();
        }
    }
}