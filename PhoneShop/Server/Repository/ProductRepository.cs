﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using PhoneShop.Server.Data;
using PhoneShop.Shared.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace PhoneShop.Server.Repository
{
    public class ProductRepository : IProductRepository
    {
        private readonly ApplicationDbContext _appDbContext;
        private readonly ILogger<ApplicationDbContext> _logger;

        private static readonly int _pageSize = 9;

        public ProductRepository(ApplicationDbContext appDbContext, ILogger<ApplicationDbContext> logger)
        {
            _appDbContext = appDbContext;
            _logger = logger;
        }

        public Product AddProduct(Product product)
        {
            try
            {
                _appDbContext.Product.Add(product);
                int n = _appDbContext.SaveChanges();
                return product;
            } catch (Exception e)
            {
                _logger.Log(LogLevel.Information, "Addroduct fail with error: " + e);
                return null;
            }
        }

        public IEnumerable<Product> GetAllProduct()
        {
            try
            {
                return _appDbContext.Product.ToList();
            } catch(Exception e)
            {
                _logger.Log(LogLevel.Information, "GetAllProduct fail with error: " + e);
                return null;
            }
           
        }

       
        public IEnumerable<Product> GetFilterProduct(string keyword, double minPrice, double maxPrice, int brandId,
            int page)
        {

            try
            {
                string filterByBrand = "";
                if (brandId > 0)
                {
                    filterByBrand = " and brandId = " + brandId;
                }

                IEnumerable<Product> producList;
                producList = _appDbContext.Product
                    .FromSqlRaw("SELECT * FROM Product " +
                                "WHERE price >= {0} and price <= {1} and name like {2} and status = 1 and quantity > 0 " + filterByBrand +
                                "ORDER by Product.price DESC " +
                                "OFFSET {3} rows " +
                                "FETCH NEXT {4} rows ONLY", minPrice, maxPrice, "%" + keyword + "%",
                        _pageSize * page - _pageSize, _pageSize)
                    .ToList();
                return producList;
            } catch(Exception e)
            {
                _logger.Log(LogLevel.Information, "GetFilterProduct fail with error: " + e);

                return null;
            }
           
        }
        public IEnumerable<Product> GetFilterProductAdmin(string keyword, double minPrice, double maxPrice, int brandId,
          int page)
        {

            try
            {
                string filterByBrand = "";
                if (brandId > 0)
                {
                    filterByBrand = " and brandId = " + brandId;
                }

                IEnumerable<Product> producList;
                producList = _appDbContext.Product
                    .FromSqlRaw("SELECT * FROM Product " +
                                "WHERE price >= {0} and price <= {1}  and name like {2} " + filterByBrand +
                                "ORDER by Product.price DESC " +
                                "OFFSET {3} rows " +
                                "FETCH NEXT {4} rows ONLY", minPrice, maxPrice, "%" + keyword + "%",
                        _pageSize * page - _pageSize, _pageSize)
                    .ToList();
                return producList;
            }
            catch (Exception e)
            {
                _logger.Log(LogLevel.Information, "GetFilterProduct fail with error: " + e);

                return null;
            }

        }
        public Product UpdateProduct(Product product)
        {
            try
            {
                _appDbContext.Entry(product).State = EntityState.Modified;
                _appDbContext.SaveChanges();
                return product;
            } catch(Exception e)
            {
                _logger.Log(LogLevel.Information, "UpdateProduct fail with error: " + e);
                return null;
            }
         
        }

        /// <summary>
        /// Return a product object given productId
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        public Product GetProduct(int productId)
        {
            try
            {
                return _appDbContext.Product
               .FirstOrDefault(product => product.Id == productId);
            } catch(Exception e)
            {
                _logger.Log(LogLevel.Information, "GetProduct fail with error: " + e);
                return null;
            }
           
        }
        
    }
}