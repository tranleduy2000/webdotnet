﻿using Microsoft.EntityFrameworkCore;
using PhoneShop.Server.Data;
using PhoneShop.Shared.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace PhoneShop.Server.Repository
{
    public class BrandRepository : IBrandRepository
    {
        private readonly ApplicationDbContext _appDbContext;

        public BrandRepository(ApplicationDbContext applicationDb)
        {
            _appDbContext = applicationDb;
        }

        public Brand AddBrand(Brand brand)
        {
            int maxId = _appDbContext.Brand.Max(temp => temp.Id);
            brand.Id = maxId + 1;
            brand.Status = true;
            _appDbContext.Brand.Add(brand);
            _appDbContext.SaveChanges();
            return brand;
        }

        public IEnumerable<Brand> GetAllBrand()
        {
            IEnumerable<Brand> brandList = _appDbContext.Brand
                .FromSqlRaw("SELECT * FROM Brand WHERE status = 1")
                .ToList();
            return brandList;
        }

        public Brand UpdateBrand(Brand brand)
        {
            _appDbContext.Brand.Update(brand);
            _appDbContext.SaveChanges();
            return brand;
        }

        public bool DeleteBrand(int brandId)
        {
            Brand brand = _appDbContext.Brand.Find(brandId);
            brand.Status = false;
            _appDbContext.Brand.Update(brand);
            return _appDbContext.SaveChanges() > 0;
        }

        public Brand GetBrand(int brandId)
        {
            return _appDbContext.Brand.Find(brandId);
        }
    }
}
