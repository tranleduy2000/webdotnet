﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using PhoneShop.Server.Data;
using PhoneShop.Shared.Model;

namespace PhoneShop.Server.Repository
{
    public class OrderRepository : IOrderRepository
    {
        private readonly ApplicationDbContext _applicationDbContext;
        private readonly ILogger<ApplicationDbContext> _logger;

        public OrderRepository(ApplicationDbContext applicationDbContext, ILogger<ApplicationDbContext> logger)
        {
            _applicationDbContext = applicationDbContext;
            _logger = logger;
        }

        public IEnumerable<Orders> GetAllOrders()
        {
            _logger.Log(LogLevel.Information, "GetAllOrders called");
            return _applicationDbContext.Orders.ToList();
        }

        // https://stackoverflow.com/questions/9410321/using-select-and-where-in-a-single-linq-statement
        public IEnumerable<Orders> GetOrdersByUserId(string userId)
        {
            _logger.Log(LogLevel.Information, "GetOrdersByUserId called with " + userId);
            var orders = _applicationDbContext.Orders
                .Where(order => order.UserId == userId)
                .ToList();
            foreach (var order in orders)
            {
                if (order.User != null)
                {
                    order.User.Orders = null;
                }
            }

            return orders;
        }

        public Orders GetOrder(int orderId)
        {
            var order = _applicationDbContext.Orders
                .FirstOrDefault(orders => orders.Id == orderId);
            return order;
        }

        public IEnumerable<OrderDetail> GetOrderDetails(int orderId)
        {
            List<OrderDetail> orderDetails = _applicationDbContext.OrderDetail
                .Include(o => o.Product)
                .Where(detail => detail.Order.Id == orderId)
                .ToList();
            foreach (var orderDetail in orderDetails)
            {
                orderDetail.Product.OrderDetail = null; // remove reference
                orderDetail.Order = null;
            }
            return orderDetails;
        }

        public Orders InsertOrder(Cart cart, OrderInfo orderInfo)
        {
            if (!CheckStock(cart))
            {
                return null;
            }

            _applicationDbContext.Database.BeginTransaction();
            try
            {
                Orders order = new Orders
                {
                    Time = DateTime.Now,
                    FullName = orderInfo.FullName,
                    PhoneNumber = orderInfo.Phone,
                    UserId = orderInfo.UserId,
                    Address = orderInfo.Address,
                    Status = true,
                    Type = 1,
                    PaymentStatus = 0,
                    PaymentMethod = "COD"
                };
                _applicationDbContext.Orders.Add(order);
                _applicationDbContext.SaveChanges();
                foreach (Cart.CartItem item in cart.CartItems)
                {
                    OrderDetail orderDetail = new OrderDetail
                    {
                        OrderId = order.Id,
                        ProductId = item.Product.Id,
                        Quantity = item.Quantity
                    };
                    var product = _applicationDbContext.Product.First(p => p.Id == item.Product.Id);
                    product.Quantity -= item.Quantity;

                    _applicationDbContext.OrderDetail.Add(orderDetail);
                    _applicationDbContext.Product.Update(product);
                    _applicationDbContext.SaveChanges();
                }

                _applicationDbContext.Database.CommitTransaction();
                return order;
            }
            catch (Exception ex)
            {
                _applicationDbContext.Database.RollbackTransaction();
                return null;
            }
        }

        /// <summary>
        /// Check if the number of products in database is enough for create order
        /// </summary>
        public bool CheckStock(Cart cart)
        {
            if (cart == null)
            {
                return false;
            }

            foreach (var item in cart.CartItems)
            {
                Product product = _applicationDbContext.Product.First(p => p.Id == item.Product.Id);
                if (product.Quantity < item.Quantity)
                {
                    return false;
                }
            }

            return true;
        }
    }
}