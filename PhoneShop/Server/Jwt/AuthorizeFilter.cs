﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace PhoneShop.Server.Jwt
{
    public class AuthorizeFilter : IAuthorizationFilter
    {
        private readonly string[] _claims;

        public AuthorizeFilter(params string[] claim)
        {
            _claims = claim;
        }

        public void OnAuthorization(AuthorizationFilterContext context)
        {
            var isAuthenticated = context.HttpContext.User.Identity.IsAuthenticated;
            // var claimsIndentity = context.HttpContext.User.Identity as ClaimsIdentity;

            if (isAuthenticated)
            {
                var isAllowAccess = false;
                if (_claims.Length > 0)
                {
                    foreach (var item in _claims)
                    {
                        if (context.HttpContext.User.HasClaim("userRole", item))
                        {
                            isAllowAccess = true;
                        }
                    }
                }
                else
                {
                    isAllowAccess = true;
                }

                //if role is acrequire JWTToke
                if (!isAllowAccess)
                {
                    context.Result = new UnauthorizedResult();
                }
            }
            else
            {
                context.Result = new UnauthorizedResult();
            }
        }
    }
}