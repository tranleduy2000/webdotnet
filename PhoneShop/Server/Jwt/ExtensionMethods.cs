﻿using System.Collections.Generic;
using System.Linq;
using PhoneShop.Shared.Model;

namespace PhoneShop.Server.Jwt
{
    public static class ExtensionMethods
    {

        public static Users WithoutPassword(this Users user) 
        {
            if (user == null) return null;

            user.Password = null;
            return user;
        }
    }
}