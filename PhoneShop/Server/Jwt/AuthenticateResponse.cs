﻿using PhoneShop.Shared.Model;

namespace PhoneShop.Server.Jwt
{
    public class AuthenticateResponse
    {
        public string UserId { get; set; }
        public int RoleId { get; set; }
        private string Role { get; set; }
        public string Token { get; set; }


        public AuthenticateResponse(Users user, string token)
        {
            UserId = user.Id;
            Role = "Undefined";
            if (user.RoleId != null)
            {
                RoleId = user.RoleId.Value;
                Role = Shared.Model.Role.GetRoleName(RoleId);
            }
            Token = token;
        }
    }
}