﻿using System.ComponentModel.DataAnnotations;

namespace PhoneShop.Shared.Model
{
    public class PasswordModel
    {
        [Required]
        public string OldPassword { get; set; }
        
        [Required]
        public string NewPassword { get; set; }
    }
}