﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace PhoneShop.Shared.Model
{
    public partial class Role
    {
        public Role()
        {
            Users = new HashSet<Users>();
        }

        [Key]
        [Column("id")]
        public int Id { get; set; }
        [Column("name")]
        [StringLength(50)]
        public string Name { get; set; }
        [Column("status")]
        public bool? Status { get; set; }

        [InverseProperty("Role")]
        public virtual ICollection<Users> Users { get; set; }

    }
}
