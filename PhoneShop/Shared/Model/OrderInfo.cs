﻿namespace PhoneShop.Shared.Model
{
    public class OrderInfo
    {
        public string FullName { get; set; }     
        public string Phone { get; set; }
        public string Address { get; set; }
        public string PaymentMethod { get; set; }
        public string UserId { get; set; }
    }
}