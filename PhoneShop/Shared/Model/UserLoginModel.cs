﻿using System.ComponentModel.DataAnnotations;

namespace PhoneShop.Shared.Model
{
    public class UserLoginModel
    {
        [Required] public string UserName { get; set; }

        [Required]
        [StringLength(50, MinimumLength = 1, 
            ErrorMessage = "Password must contain 8-16 character")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        public UserLoginModel()
        {
        }

        public UserLoginModel(string userName, string password)
        {
            UserName = userName;
            Password = password;
        }
    }
}