﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace PhoneShop.Shared.Model
{
    public partial class Orders
    {
        public Orders()
        {
            OrderDetail = new HashSet<OrderDetail>();
        }

        [Key]
        [Column("id")]
        public int Id { get; set; }
        [Column("fullName")]
        [StringLength(100)]
        public string FullName { get; set; }
        [Column("address")]
        [StringLength(200)]
        public string Address { get; set; }
        [Column("phoneNumber")]
        [StringLength(12)]
        public string PhoneNumber { get; set; }
        [Column("userId")]
        [StringLength(50)]
        public string UserId { get; set; }
        [Column("time", TypeName = "datetime")]
        public DateTime? Time { get; set; }
        [Column("type")]
        public int? Type { get; set; }
        [Column("paymentStatus")]
        public int? PaymentStatus { get; set; }
        [Column("paymentMethod")]
        [StringLength(100)]
        public string PaymentMethod { get; set; }
        [Column("status")]
        public bool? Status { get; set; }

        [ForeignKey(nameof(UserId))]
        [InverseProperty(nameof(Users.Orders))]
        public virtual Users User { get; set; }
        [InverseProperty("Order")]
        public virtual ICollection<OrderDetail> OrderDetail { get; set; }
    }
}
