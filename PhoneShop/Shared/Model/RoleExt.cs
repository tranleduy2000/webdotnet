﻿namespace PhoneShop.Shared.Model
{
    public partial class Role
    {
        public const string Admin = "Admin";
        public const string User = "User";
        
        public const int AdminId = 1;
        public const int UserId = 0;
        
        public static string GetRoleName(int roleId)
        {
            string role = "";

            if (roleId == 1)
                role = Role.Admin;
            else
                role = Role.User;
            return role;
        }
    }
}