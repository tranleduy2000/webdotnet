﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PhoneShop.Shared.Model
{
    public class Cart
    {
        public List<CartItem> CartItems { get; set; }

        public Cart()
        {
            CartItems = new List<CartItem>();
        }

        public void Remove(int productId)
        {
            CartItems.RemoveAll(item => item.Product.Id == productId);
        }

        public double GetTotal()
        {
            return Convert.ToDouble(CartItems.Sum(item => item.Quantity * item.Product.Price).ToString());
        }

        public CartItem Get(int productId)
        {
            foreach (var cartItem in CartItems)
            {
                if (cartItem.Product.Id == productId)
                {
                    return cartItem;
                }
            }

            return null;
        }

        public int Size()
        {
            return CartItems.Count;
        }

        public class CartItem
        {
            public Product Product { get; set; }
            public int Quantity { get; set; }

            public CartItem()
            {
            }

            public CartItem(Product product, int quantity = 0)
            {
                Product = product;
                Quantity = quantity;
            }

            protected bool Equals(CartItem other)
            {
                return Equals(Product.Id, other.Product.Id);
            }

            public override bool Equals(object obj)
            {
                if (ReferenceEquals(null, obj)) return false;
                if (ReferenceEquals(this, obj)) return true;
                if (obj.GetType() != this.GetType()) return false;
                return Equals((CartItem) obj);
            }

            public override int GetHashCode()
            {
                return (Product != null ? Product.Id.GetHashCode() : 0);
            }
        }

        public void Add(CartItem cartItem)
        {
            CartItems.Add(cartItem);
        }
    }
}