﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace PhoneShop.Shared.Model
{
    public partial class Users
    {
        public Users()
        {
            Orders = new HashSet<Orders>();
        }
        
        [Required]
        [Key]
        [Column("id")]
        [StringLength(50)]
        public string Id { get; set; }
        
        [Required]
        [Column("password")]
        public string Password { get; set; }
        
        
        [Column("status")]
        public bool? Status { get; set; }
        [Required]
        [Column("roleId")]
        public int? RoleId { get; set; }
        
        [Required]
        [Column("fullName")]
        [StringLength(100)]
        public string FullName { get; set; }
        
        [Required]
        [Column("phone")]
        [StringLength(20)]
        public string Phone { get; set; }
        
        [Required]
        [Column("address")]
        [StringLength(200)]
        public string Address { get; set; }

        [ForeignKey(nameof(RoleId))]
        [InverseProperty("Users")]
        public virtual Role Role { get; set; }
        [InverseProperty("User")]
        public virtual ICollection<Orders> Orders { get; set; }

        public Users(string id, string password, string fullName, string phone, string address)
        {
            Id = id;
            Password = password;
            FullName = fullName;
            Phone = phone;
            Address = address;
        }
    }
}
