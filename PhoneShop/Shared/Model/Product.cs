﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace PhoneShop.Shared.Model
{
    public partial class Product
    {
        public Product()
        {
            OrderDetail = new HashSet<OrderDetail>();
        }

        [Key]
        [Column("id")]
        public int Id { get; set; }
        [Column("name")]
        [StringLength(200, ErrorMessage = "Identifier too long (200 character limit).")]
        [Required]
        public string Name { get; set; }
        [Column("status")]
        public bool? Status { get; set; }
        [Required]
        [Column("price", TypeName = "decimal(18, 3)")]
        [Range(1, 100000000, ErrorMessage = "Price must be in range (1 - 100.000.000).")]
        public decimal? Price { get; set; }
        [Column("brandId")]
        public int? BrandId { get; set; }
        [Column("description")]
        public string Description { get; set; }
        [Column("quantity")]
        [Required]
        [Range(1, 1000000, ErrorMessage = "Quantity must be in range (1 - 1000000).")]
        public int? Quantity { get; set; }
        [Column("imagePath")]
        [StringLength(200)]
        [Required]
        public string ImagePath { get; set; }

        [ForeignKey(nameof(BrandId))]
        [InverseProperty("Product")]
        public virtual Brand Brand { get; set; }
        [InverseProperty("Product")]
        public virtual ICollection<OrderDetail> OrderDetail { get; set; }
    }
}
