﻿using System.ComponentModel.DataAnnotations;

namespace PhoneShop.Shared.Model
{
    public class AccountChangingInfoModel
    {
        [Required]
        public string FullName { get; set; }
        
        [Required]
        public string Address { get; set; }
        
        [Required]
        public string Phone { get; set; }
    }
}