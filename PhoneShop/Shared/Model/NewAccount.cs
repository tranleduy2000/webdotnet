﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PhoneShop.Shared.Model
{
    public class NewAccount
    {
        [Required]
        public string Id { get; set; }
        
        [Required]
        public string Password { get; set; }
        
        
        [Required]
        public string FullName { get; set; }
        
        [Required]
        public string Phone { get; set; }
        
        [Required]
        public string Address { get; set; }
    }
}