﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace PhoneShop.Shared.Model
{
    [Index(nameof(OrderId), nameof(ProductId), Name = "OrderDetail_pk", IsUnique = true)]
    public partial class OrderDetail
    {
        [Key]
        [Column("orderId")]
        public int OrderId { get; set; }
        [Key]
        [Column("productId")]
        public int ProductId { get; set; }
        [Column("quantity")]
        public int? Quantity { get; set; }

        [ForeignKey(nameof(OrderId))]
        [InverseProperty(nameof(Orders.OrderDetail))]
        public virtual Orders Order { get; set; }
        [ForeignKey(nameof(ProductId))]
        [InverseProperty("OrderDetail")]
        public virtual Product Product { get; set; }
    }
}
