# How to run
1. Run `DatabaseScript.sql`
2. Change `sa` password=`123456`


# Resource
| Description | Link |
| ------ | ------ |
|Image view|http://blog.vivensas.com/static-image-in-blazor-inside-and-outside-web-root/|
|Communicate between components |https://chrissainty.com/3-ways-to-communicate-between-components-in-blazor/|
|Error click two time to submit form|https://stackoverflow.com/questions/56436577/blazor-form-submit-needs-two-clicks-to-refresh-view|
|Calling non parameter API doesn't work as expected |https://github.com/dotnet/aspnetcore/issues/20541| 
|Validate Annotation in Model|https://www.telerik.com/blogs/first-look-forms-and-validation-in-razor-components|
|Using component and razor page example|https://www.codewithmukesh.com/blog/blazor-crud-with-entity-framework-core/|
|Why we using await? Simple explaination|https://stackoverflow.com/questions/30042791/entity-framework-savechanges-vs-savechangesasync-and-find-vs-findasync|
|Select with WHERE in single Linq statement|https://stackoverflow.com/questions/9410321/using-select-and-where-in-a-single-linq-statement|
|Blind input component|https://docs.microsoft.com/en-us/aspnet/core/blazor/components/data-binding?view=aspnetcore-5.0|
|Select with WHERE in single Linq statement|https://stackoverflow.com/questions/9410321/using-select-and-where-in-a-single-linq-statement|
|Upload file|https://www.mikesdotnetting.com/article/341/uploading-files-in-blazor|