﻿
# Install dotnet-ef command lime tools
dotnet tool install --global dotnet-ef --version 5.0.0-rc.2.20475.6
# Or update from existing version
# dotnet tool update --global dotnet-ef --version 5.0.0-rc.2.20475.6

# dbcontext required ef version 5+
dotnet ef --version

# https://docs.microsoft.com/en-us/ef/core/miscellaneous/cli/dotnet#dotnet-ef-dbcontext-scaffold
# Move to Server project
cd PhoneShop/Server
# Upgrade libraries
dotnet add package Microsoft.EntityFrameworkCore --version 5.0.0-rc.2.20475.6
dotnet add package Microsoft.EntityFrameworkCore.SqlServer --version 5.0.0-rc.2.20475.6
dotnet add package Microsoft.EntityFrameworkCore.Design --version 5.0.0-rc.2.20475.6
dotnet add package Microsoft.EntityFrameworkCore.Tools --version 5.0.0-rc.2.20475.6
dotnet add package Microsoft.EntityFrameworkCore.Abstractions --version 5.0.0-rc.2.20475.6
dotnet add package Microsoft.AspNetCore.Identity.UI --version 5.0.0-rc.2.20475.17
dotnet add package Microsoft.Extensions.Caching.Memory --version  5.0.0-rc.2.20475.5

# Run the following command to generate models from database.
dotnet ef dbcontext scaffold "Server=.;Database=PhoneShop;Trusted_Connection=True;" Microsoft.EntityFrameworkCore.SqlServer --context "ApplicationDbContext" --context-namespace "PhoneShop.Server.Data" --context-dir "./Data" --output-dir "../Shared/Model" --namespace "PhoneShop.Shared.Model" --data-annotations --force --no-pluralize --no-onconfiguring

#install in PM in VS to using Cart API
Install-Package Microsoft.AspNetCore.Mvc.NewtonsoftJson