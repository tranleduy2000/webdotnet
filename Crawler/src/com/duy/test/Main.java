package com.duy.test;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.chrome.ChromeOptions;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Random;

public class Main {

    private static final String BASE_URL = "https://www.thegioididong.com";
    private static final String PHONE_LIST_URL = "https://www.thegioididong.com/dtdd#i:6";
    private static File buildDir;
    private static Random random = new Random(System.currentTimeMillis());

    public static void main(String[] args) throws IOException {
        buildDir = new File("./build");
        buildDir.mkdirs();

        System.setProperty("webdriver.chrome.driver", "./chromedriver.exe");
        ChromeOptions options = new ChromeOptions();
        options.setHeadless(true);
        // WebDriver driver = new ChromeDriver(options);
        //  driver.get(BASE_URL);
        //  String content = driver.getPageSource();
        //  FileUtils.write(mainPageFile, content);
        //  driver.close();

        File mainPageFile = new File(buildDir, "tgdd.html");
        File imageDir = new File(buildDir, "images");
        File sqlDir = new File(buildDir, "script");
        File htmlDir = new File(buildDir, "htmls");
        imageDir.mkdirs();
        sqlDir.mkdirs();

        StringBuilder sqlScript = new StringBuilder();
        addClearTablesScript(sqlScript);
        addInsertBrandDataScript(sqlScript);

        Document rootDocument = Jsoup.parse(FileUtils.readFileToString(mainPageFile));
        Elements items = rootDocument.select(".item");
        for (Element item : items) {
            String price = item.select(".price").select("strong").text().replace(".", "").replace("₫", "");
            String name = item.select("h3").first().text();

            Element imgTag = item.select("img").first();
            String imageUrl = imgTag.attr("src");
            if (imageUrl.trim().isEmpty()) {
                imageUrl = imgTag.attr("data-original");
            }
            File imageFile = new File(imageDir, imageUrl.substring(imageUrl.lastIndexOf("/") + 1));
            // downloadFIle(imageUrl, imageFile);

            String phoneInfoUrl = BASE_URL + item.select("a").first().attr("href");
            System.out.println("phoneInfoUrl = " + phoneInfoUrl);
            // driver.get(phoneInfoUrl);
            // driver.manage().timeouts().implicitlyWait(1, TimeUnit.MINUTES);
            Element phoneInfoPage = getHtml(phoneInfoUrl, htmlDir);
            Elements areaArticleTag = phoneInfoPage.select(".area_article");
            areaArticleTag.select(".boxRtAtc").remove();

            String description = areaArticleTag.text().replace("'", "''");

            int quantity = random.nextInt(100);
            sqlScript.append(String.format("INSERT Product(name,price,description,imagePath,quantity) " +
                    "VALUES(N'%s',%s,N'%s','%s',%d)", name, price, description, imageFile.getName(), quantity))
                    .append("\n");
        }

        FileUtils.write(new File(sqlDir, "script.sql"), sqlScript);
    }

    private static Element getHtml(String url, File cacheDir) throws IOException {
        File file = new File(cacheDir,
                url.replace(":", "").replace("\\", "")
                        + ".html");
        if (file.exists() && file.length() > 0) {
            return Jsoup.parse(FileUtils.readFileToString(file));
        }
        Document document = Jsoup.connect(url).timeout(60000).get();
        String html = document.toString();
        FileUtils.write(file, html);
        return document;
    }

    private static void addInsertBrandDataScript(StringBuilder sqlScript) {

    }

    private static void addClearTablesScript(StringBuilder sqlScript) {
        sqlScript.append("DELETE Product WHERE id IS NOT NULL;\n");
        sqlScript.append("DELETE Brand WHERE id IS NOT NULL;\n");
    }

    private static void downloadFIle(String url, File file) throws IOException {
        System.out.println("Main.downloadFIle");
        System.out.println("url = " + url);
        URLConnection urlConnection = new URL(url).openConnection();
        FileOutputStream output = new FileOutputStream(file);
        IOUtils.copy(urlConnection.getInputStream(), output);
        output.close();
    }

    // id
    //name
    //status
    //price
    //brandId
    //description
    //quantity
}
